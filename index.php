<?php
	/**
	 * Initialise a data array with a sequential set of integers, then shuffle to create an un-sorted 
	 * list for the purpose of this example..
	 */
	for ($i = 1; $i <= 20; $i++) {
		$data[] = $i;
	}

	shuffle($data);

	// Output the initially un-sorted list.
	echo "<pre>";
	print_r($data);
	echo "</pre>";

	// Determine the start time of the function.
	$start = microtime(true);

	// Sort the data.
	$sortedData = bubbleSort($data);

	// Calculate the time taken to execute the sort function
	$duration = microtime(true) - $start;

	echo "Duration : $duration s";

	// Output the sorted data.
	echo "<pre>";
	print_r($sortedData);
	echo "</pre>";

	/**
	 * Perform a bubble sort on an array of simple data types, e.g. integers.
	 *
	 * @param array $data This is an array of simple data types, e.g. integers
	 * @return array
	 */
	function bubbleSort($data) {
		/**
		 * Iterate through each element of the array, comparing it's value with all 
		 * other elements of the array.
		 */
		for ($i = 0; $i < count($data); $i++) {
			for ($j = 0; $j < count($data); $j++) {
				if ($data[$i] < $data[$j]) {
					/**
					 * The initial value is less than the value it has been compared 
					 * with. The position of the values should be switched.
					 */
					$tmpValue = $data[$i];
					$data[$i] = $data[$j];
					$data[$j] = $tmpValue;
				}
			}
		}

		return $data;
	}
?>